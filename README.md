# tools: Teaching / Research Tools

## Description

A collection of web-based tools to support my teaching / research activities. 

### Clock

A basic webpage that displays a clock (hours and minutes only, in 24hr time)

Features:
- **Adjustment buttons**: the clock is based on system time, and the computer installed on the display podium may have time that disagrees with classroom standard. The adjustments allows for the displayed time to be changed without needing to change the computer time. 
- **Note field**: a notes field is implemented via a `contenteditable` so that notes can be added and be shown to the viewers. Use cases include: message to audience members before the presentation starts; or recording the anticipated start/end time of an exam when the clock is used to keep track of time during a timed/proctored exam. 




## License

All code in this repository is released AS IS into the PUBLIC DOMAIN. 
